function login(username, password, role){
    if(!username || !password || !role){
        console.log("Inputs must not be empty");
    } else if(role) {
        switch (role) {
            case 'admin':
                return "Welcome back to the class portal, admin!"
                break;
            case 'teacher':
                return "Thank you for logging in, teacher!"
                break;
            case 'student':
                return "Welcome back to the class portal, student!"
                break;
            default:
                return "Role out of range."
                break;
        }
    }
}

function checkAverage(num1,num2,num3,num4){
    let average = Math.round((num1 + num2 + num3 + num4)/4);
    
    if(average <= 74){
        return "Hello, student, your average is " + average + ". The letter equivalent is F"
    } else if (average >= 75 && average <= 79) {
        return "Hello, student, your average is " + average + ". The letter equivalent is D"
    } else if (average >= 80 && average <= 84) {
        return "Hello, student, your average is " + average + ". The letter equivalent is C"
    } else if (average >= 85 && average <= 89) {
        return "Hello, student, your average is " + average + ". The letter equivalent is B"
    } else if (average >= 90 && average <= 95) {
        return "Hello, student, your average is " + average + ". The letter equivalent is A"
    } else if (average >= 96) {
        return "Hello, student, your average is " + average + ". The letter equivalent is A+"
    }
}

login()
login("","password", "admin");
login("AdminUser","", "admin");
login("AdminUser","password", "");
console.log(login("adminUser", "password", "admin"));
console.log(login("adminUser", "password", "teacher"));
console.log(login("adminUser", "password", "student"));
console.log(login("adminUser", "password", "carpenter"));

console.log(checkAverage(71,70,73,71));
console.log(checkAverage(75,75,76,78));
console.log(checkAverage(80,82,83,81));
console.log(checkAverage(85,86,85,86));
console.log(checkAverage(91,90,92,90));
console.log(checkAverage(95,96,97,96));



try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}