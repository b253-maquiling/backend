const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const auth = require("../middleware/auth");

// @desc Register a user
// @route POST /user/register
// @access public
const registerUser = asyncHandler(async (req, res) => {
  const { firstName, lastName, email, password } = req.body;

  if (!firstName || !lastName || !email || !password) {
    res.status(400).send(`All fields are mandatory!`);
  }
  const userAvailable = await User.findOne({ email });
  if (userAvailable) {
    return res.status(400).send(`${email} is already registered`);
  }

  const hashedPassword = await bcrypt.hash(password, 10);

  const user = await User.create({
    firstName,
    lastName,
    email,
    password: hashedPassword,
  });

  if (user) {
    return res.status(201).json({ _id: user.id, email: user.email });
  } else {
    return res.status(400).json("User data is not valid");
  }
});

// @desc Login user
// @route POST /user/login
// @access private
const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send("All fields are mandatory!");
  }

  const user = await User.findOne({ email });

  if (user && (await bcrypt.compare(password, user.password))) {
    const accessToken = jwt.sign(
      {
        user: {
          id: user._id,
          email: user.email,
          isAdmin: user.isAdmin,
        },
      },
      process.env.ACCESS_TOKEN_SECRET,
      {}
    );
    return res.status(200).json({
      message: `${user.email} is now loggedin`,
      token: accessToken,
    });
  } else {
    return res.status(401).send(`email or password is not valid`);
  }
});

// @desc User details
// @route GET /user/:id/userDetails
// @access private
const getUserDetails = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const userDetails = await User.findById(req.params.id);
  if (userDataToken.user.id !== userDetails.id) {
    // Can view user details because admin access
    /* if (userDataToken.user.isAdmin) {
      return res.status(200).send(userDetails);
    } */
    return res.status(403).send(`${userDataToken.user.id} is forbidden to access ${userDetails.id} Details`);
  }
  if (!userDetails) {
    return res.status(404).send(`${req.params.userId} is not found!`);
  } else {
    userDetails.password = "";
    return res.status(200).send(userDetails);
  }
});

// @desc Change/Set user to admin
// @route PUT /user/:id/setAsAdmin
// @access private
const setAsAdmin = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const userDetails = await User.findById(req.params.id);

  if (!userDetails) {
    return res.status(404).send("No user found!");
  }

  if (userDataToken.user.isAdmin) {
    const setUserAdmin = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.status(200).json(setUserAdmin);
  } else {
    return res.status(401).json({ auth: false });
  }
});

module.exports = { registerUser, loginUser, getUserDetails, setAsAdmin };
