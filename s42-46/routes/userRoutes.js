const express = require("express");
const {
  createOrder,
  getOrders,
  getMyOrders,
  removeOrder,
  checkoutPay,
} = require("../controllers/orderController");
const {
  registerUser,
  loginUser,
  getUserDetails,
  setAsAdmin,
} = require("../controllers/userController");
const { verify } = require("../middleware/auth");

const router = express.Router();

// User Routes
router.post("/register", registerUser);

router.post("/login", loginUser);

router.put("/:id/setAsAdmin", verify, setAsAdmin);

router.get("/:id/userDetails", verify, getUserDetails);

// Order Routes
router.use(verify);

router.post("/checkout", createOrder);

router.post("/checkout/pay", checkoutPay);

router.get("/orders", getOrders);

router.delete("/removeOrder", removeOrder)

router.get("/myOrders", getMyOrders);

module.exports = router;
