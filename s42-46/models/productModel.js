const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product name is required"],
  },
  description: {
    type: String,
    required: [true, "Product description is requried"],
  },
  price: {
    type: Number,
    required: [true, "Prodict price is requried"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createOn: {
    type: Date,
    default: new Date()
  },
  updatedOn: {
    type: Date,
    default: undefined,
  },
});

module.exports = mongoose.model("Product", productSchema);
