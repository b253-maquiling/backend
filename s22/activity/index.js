let registeredUsers = [
    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];
let friendsList = [];

function register(user){
    if(registeredUsers.includes(user)){
        return "Registration failed. Username already exists!";
    } else {
        registeredUsers.push(user) ;
        return "Thank you for registering!";
    }
}

function addFriend(friend){
    if(registeredUsers.includes(friend)){
        friendsList.push(friend);
        return "You have added " + friend + " as a friend!";
    } else {
        return "User not found";
    }
}

function displayFriends(){
    return friendsList.length <= 0 ? "You currently have 0 friends. Add one first" : friendsList.toString();
}

function displayNumberOfFriends(){
    return friendsList.length <= 0 ? "You currently have 0 friends. Add one first" : "You currently have " + friendsList.length + " friends.";
}

function deleteFriend(){
    return friendsList.length <= 0 ? "You currently have 0 friends. Add one first" : friendsList.pop()
}

function deleteSpecificFriend(index){

    return friendsList.splice(index, 1)
}


try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend
    }
}
catch(err) {

}