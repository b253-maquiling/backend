// Javascript is by default is synchronous meaning that only one statement is executed at a time
// This can be proven when a statement has an error, javascript will not proceed with the next statement

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Getting All posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

console.log(fetch(`https://jsonplaceholder.typicode.com/posts`));

/* 
    Syntax:
        fetch("url")
            .then((res) => {})
*/

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch(`https://jsonplaceholder.typicode.com/posts`)
  // The "fetch" method will return a "promise" that resolves to a "Response" object
  // The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
  .then((res) => {
    console.log(res.status);
  });

fetch("https://jsonplaceholder.typicode.com/posts")
  // Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
  // Print the converted JSON value from the "fetch" request
  // Using multiple "then" methods creates a "promise chain"
  .then((response) => response.json())
  .then((data) => console.log(data));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for

async function fetchData() {
  // waits for the "fetch" method to complete then stores the value in the "result" variable
  let result = await fetch(`https://jsonplaceholder.typicode.com/posts`);
  // Result returned by fetch returns a promise
  console.log(result);
  // The returned "Response" is an object
  console.log(typeof result);
  // We cannot access the content of the "Response" by directly accessing it's body property
  console.log(result.body);
  // Converts the data from the "Response" object as JSON
  let json = await result.json();
  // Print out the content of the "Response" object
  console.log(json);
}

fetchData();

// Getting specific post

// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then((response) => response.json())
  .then((json) => console.log(json));

// Creating a post
/* 
    Syntax:
        fetch("url", {options})
            .then((res) => {})
            .then(res => ())
*/

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "POST",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "New post",
    body: "Hello World!",
    userId: 1,
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PATCH  is used to update a single/several properties of an object
// PUT is used to update the entire object or replace it with a new one.
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PATCH",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "New post 2",
    body: "Hello World! 2",
    userId: 1,
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));

// Deleting a post
// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
});
