// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/taskModel");

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {
  // The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
  // The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
  return Task.find({})
    .then((result) => {
      return result;
    })
    .catch((err) => err);
};

module.exports.createTasks = (req) => {
  let newTasks = new Task({
    name: req.name,
  });

  return newTasks.save().then((result, error) => {
    if (error) {
      console.log(error);
    } else {
      return newTasks;
    }
  });
};

module.exports.deleteTask = (req) => {
  return Task.findByIdAndRemove(req)
    .then((result) => result)
    .catch((error) => error);
};

// Controller function for updating a task

// Business Logic
/*
			1. Get the task with the id using the Mongoose method "findById"
			2. Replace the task's name returned from the database with the "name" property from the request body
			3. Save the task
		*/
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"

module.exports.updateTask = (taskId, newContent) => {
  // The "findById" Mongoose method will look for a task with the same id provided from the URL
  // "findById" is the same as "find({"_id" : value})"
  // The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function
  return Task.findById(taskId).then((result, err) => {
    if (err) {
      console.log(err);
      return err;
    }
    result.name = newContent.name;

    return result.save().then((updatedTask, err) => {
      if (err) {
        console.log(err);
        return false;
      } else {
        return updatedTask;
      }
    });
  });
};

module.exports.getTask = (taskId) => {
  return Task.findById({ _id: taskId })
    .then((result, err) => {
      if (err) {
        console.log(err);
        return err;
      }
      return result;
    })
    .catch((err) => console.log(err));
};

module.exports.updateStatus = (taskId) => {
  return Task.findById({ _id: taskId }).then((result, err) => {
    if (err) {
      console.log(err);
      return err;
    }
    result.status = "complete";

    return result.save().then((updatedStatus, err) => {
      if (err) {
        return false;
      } else {
        return updatedStatus;
      }
    });
  });
};
