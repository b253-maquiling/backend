let x = 123, y = 456

let sum = x + y
console.log("Result of addtion operator: " + sum);

let difference = x - y
console.log("Result of subtraction operator: " + difference);

let product = x * y
console.log("Result of multiplaction operator: " + product);

let quotient = x / y
console.log("Result of division operator: " + quotient);

let remainder = x % y
console.log("Result of modulo operator: " + remainder);

// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;
// assignmentNumber = assignmentNumber + 2
assignmentNumber += 2
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2
console.log("Result of substraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2
console.log("Result of multiplaction assignment operator: " + assignmentNumber);

assignmentNumber /= 2
console.log("Result of division assignment operator: " + assignmentNumber);

assignmentNumber %= 2
console.log("Result of modulo assignment operator: " + assignmentNumber);

let mdas = 1 + 2 - 3 * 4 / 5
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2-3) * (4/5)
console.log("Result of pemdas: " + pemdas);

// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied
let z = 1

// let increment = z++
// console.log("Result of post-increment: " + increment);
// console.log("Result of pre-increment: " + z);

let decrement = z--
console.log("Result of post-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);


/*
    - Type coercion is the automatic or implicit conversion of values from one data type to another
    - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
    - Values are automatically converted from one data type to another in order to resolve operations
*/

let numA = '10'
let numB = 12
let coercion = numA + numB
console.log(coercion);
console.log(typeof coercion);

let numC = 16, numD = 14
let nonCoercion = numC + numD
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Equality operator
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

let juan = 'juan'

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log("juan" == "juan");
console.log(juan == "juan");


// Inequality
/* 
    - Checks whether the operands are not equal/have different content
    - Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log("juan" != "juan");
console.log(juan != "juan");


// Strict Equality Operator (===)
/* 
    - Checks whether the operands are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to
    - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log("juan" === "juan");
console.log(juan === "juan");


// Strict Inequality
/* 
    - Checks whether the operands are not equal/have the same content
    - Also COMPARES the data types of 2 values
*/

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log("juan" !== "juan");
console.log(juan !== "juan");

// Relational Operators
//Some comparison operators check whether one value is greater or less than to the other value.
// Has a boolean value

let a = 50, b = 65

let isGreaterThan = a > b
let isLessThan = a < b
let isGTorEqual = a >= b
let isLTorEqual = a <= b

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

let str = 'forty'
console.log(b >= str);

// logical operators
let isLegalAge = true
let isRegistered = false

let isAllRequirementsMet = isLegalAge && isRegistered
console.log("Result of logical AND operator: " + isAllRequirementsMet);

isAllRequirementsMet = isLegalAge || isRegistered
console.log("Result of logical OR operator: " + isAllRequirementsMet);

let areSomeRequirementsNotMet = !isRegistered
console.log("Result of logical NOT operator: " + areSomeRequirementsNotMet);