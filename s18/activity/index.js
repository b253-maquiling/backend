function addNum(num1, num2){
    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(num1 + num2);
}

function subNum(num1, num2){
    console.log("Displayed difference of " + num1 + " and " + num2);
    console.log(num1 - num2);
}

function multiplyNum(num1, num2){
    console.log("The product of " + num1 + " and " + num2 + ":");
    return num1 * num2;

}

function divideNum(num1, num2){
    console.log("The quotient of " + num1 + " and " + num2 + ":");
    return num1 / num2;
}

function getCircleArea(radius) {
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    let result = (Math.PI * radius**2);
    return result.toFixed(2);
}

function getAverage(num1,num2,num3,num4){
    console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
    return (num1 + num2 + num3 + num4) / 4;
}

function checkIfPassed(score, total){
    console.log("Is " + score + "/" + total + " a passing score?");
    let passingPercentage = (score / total) * 100;
    return passingPercentage >= 75;
}

addNum(5, 15);
subNum(20, 5);

let product = multiplyNum(50, 10);
console.log(product);

let quotient = divideNum(50,10);
console.log(quotient);

let circleArea = getCircleArea(15);
console.log(parseFloat(circleArea));

let averageVar = getAverage(20,40,60,80);
console.log(averageVar);

let isPassingScore = checkIfPassed(38,50);
console.log(isPassingScore);


try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}