//Functions
	
		//Parameters and Arguments

			// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

			//We also learned in the previous session that we can gather data from user input using a prompt() window.

function printInput() {
    let nickname = prompt("Enter your nickname: ")
    console.log(`Hi, ${nickname}`);
}

// printInput()

function printName(name){
    console.log(`My name is ${name}`);
}

printName("Giorno Giovanni")
printName("makpepe")

let sampleName = "Aoyama Nagisa"
printName(sampleName)

let divisibleBy = (number) => {console.log(number%8 === 0 )}

divisibleBy(64)
divisibleBy(28)

function argumentFunction(){
    console.log("This function was pased as an argument before the message is printed");
}

function invokeFunction(argumentFunction){
    argumentFunction()
}

invokeFunction(argumentFunction)

//Multiple Parameters

function createFullName(firstName, middleName, LastName){
    console.log(`${firstName} ${middleName} ${LastName}`);
}

createFullName("Juan", "Dela", "Cruz")

let firstName = "John"
let middleName = "Doe"
let LastName = "Smith"

createFullName(firstName, middleName, LastName)


function returnFullName(firstName, middleName, LastName){
    return firstName + " " + middleName + " " + LastName

    console.log("Cute ko");

    // Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
}
let completeName = returnFullName("Lucy", " ", "Pevensie")
console.log(completeName);

function returnAddress(city, country){
    let fullAddress = city + ", " + country
    return fullAddress
}

let address = returnAddress("Shire", "Middle Earth")
console.log(address);

function printPlayerInfo(username, level, jobClass){
    // console.log("Username: " + username);
    // console.log("Level: " + level);
    // console.log("Job: " + jobClass);
    return `Username: ${username} \n Level: ${level} \n Job: ${jobClass}`
}

let user1 = printPlayerInfo("janicchi", 95, "Rogue")
console.log(user1);

//returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

			//You cannot save any value from printPlayerInfo() because it does not return anything.