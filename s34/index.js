// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to easily create a server
const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
// For our application server to run, we need a port to listen to
const app = express();

const port = 4000;

// Setup for allowing the server to handle data from requests
// Allows your app to read json data
// Methods used from express JS are middlewares
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
// API management is one of the common application of middlewares.s
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({ extended: true }));

// ROUTES
// Express has methods corresponding to each HTTP method
// This route expects to receive a GET request at the base URI "/"
// The full base URI for our local application for this route will be at "http://localhost:3000"
// This route will return a simple message back to the client

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.post("/hello", (req, res) => {
  res.send(`Hello there ${req.body.firstname}  ${req.body.lastname}!`);
});

// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock databases
let users = [];

app.post("/signup", (req, res) => {
  const { username, password } = req.body;

  if (username && password) {
    users.push(req.body);
    res.send(`User ${username} successfully registered`);
  } else {
    res.send("Please input BOTH username and password");
  }
});

// This route expects to receive a POST request at the URI "/signup"
// This will create a user object in the "users" variable that mirrors a real world registration process
app.put("/change-password", (req, res) => {
  let message;
  const { username, password } = req.body;
  users.forEach((users) => {
    console.log(users);
    if (username === users.username) {
      users.password = password;
      message = `User ${username}'s password has been updated`;
    } else {
      message = "User does not exist.";
    }
  });
  res.send(message);
});

app.get("/home", (req, res) => {
  res.send(`Welcome to the home page`);
});

app.get("/users", (req, res) => {
  res.send(users);
});

app.delete("/delete-user", (req, res) => {
  let message;

  if (users.length > 0) {
    for (let i = 0; i < users.length; i++) {
      if (req.body.username == users[i].username) {
        users.splice(users[i], 1);

        message = `User ${req.body.username} has been deleted.`;

        break;
      }
      if (!message) {
        message = "User does not exist.";
      }
    }
  } else {
    message = "No user found.";
  }

  res.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
