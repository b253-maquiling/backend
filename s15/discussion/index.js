// Comments:

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

// Single Line Comment

/*
Multi
Line
Comment
*/

/*
	There are two types of comments:
	1. Single-line comment = denoted by two slashes 
	2. Multi-line comment - denoted by a forward slash and two asterisks

*/


console.log("Hello Batch 253!");


/*

Important Note:
	- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
	- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
	- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
	- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
	- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
*/


/*
	Syntax - This is the grammar of a language
	Statements - this is the commands or instructions for the computer

*/

// [SECTION] variables

// It is used to contain data
// Any information that is used by an application is stored in what we call a memory.
// When we create variables, certain portions of a device's memory is given a "name" that we call variables
// This makes it easier for us associate information stored in our devices to actual "names" about information.
// The names of a variables is called "identifier"

// Declaring variables:

// Declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

/*
	Syntax:
		let/const variableName
*/

//let is a keyword that is usually used in declaring a variable.
let feelings;
/*let feelings = true;

console.log(feelings);

console.log(hello);

let hello;*/

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

	Best practices in naming variables:

		1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

			let firstName = "Michael"; - good variable name
			let pokemon = 25000; - bad variable name

		2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

			let FirstName = "Michael"; - bad variable name
			let firstName = "Michael"; - good variable name

		3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

			let first name = "Mike";

		camelCase is when we have first word in small caps and the next word added without space but is capitalized:

			lastName emailAddress mobileNumber

		Underscores sample:

		let product_description = "lorem ipsum"
		let product_id = "250000ea1000"

*/
// Initializing Variables
// Initializing variables - the instance when a variable is given it's initial/starting value
// Syntax
    // let/const variableName = value;
let	productName = "desktop computer";
console.log(productName)

let	productPrice = 18999;
console.log(productPrice);

const	interest = 3.539;

// let variable cannot be re-declared within its scope.
let friend = "Owen";
// let/const friend; error identifier'friend has already been declared
friend = "Blessy";  /* re-assigned/redeclared variable*/
console.log(friend)

//while const cannot be updated or re-declared
// Values of constants cannot be changed and will simply return an error
//interest = 3.14159265; TypeError: Assignment to constant variable.
console.log(interest);

let supplier;

// This is still an initialization of variable since it is the first time assigning its value
supplier = "John Smith Tradings";

// This is considered as reassignment because its initial value was already declared
supplier = "Zuitt Store"

console.log(Math.PI);

//There are issues associated with variables declared with var, regarding with hoisting.
//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
//Hoisting is JavaScript's default behavior of moving declarations to the top.

/* let outerVar = "hello from the outside"

{
    let innerVar = "hello from inside"
}

console.log(outerVar);
console.log(innerVar); */


/* [SECTION] Data Types */

// Strings
		// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
		// Strings in JavaScript can be written using either a single (') or double (") quote
		// In other programming languages, only the double quotes can be used for creating strings

let country = 'Philippines'
let province = "Metro Manila"

// Concatenating strings
		// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country
console.log(fullAddress);

let greeting = "I live in the country" + country
console.log(greeting);

console.log("My name is Damiel " + greeting);

let mailAddress = "Metro Manila\n\nPhilippines"
console.log(mailAddress)

let message = "John's employees went home early today"
console.log(message);


// Numbers
let headcount = 26
console.log(headcount);

let grade = 98.7
console.log(grade);

let planetDistance = 2e10
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);

// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false
let inGoodConduct = false
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + inGoodConduct);

//Arrays
//Arrays are a special kind of data type that's used to store multiple values
//Arrays can store different data types BUT is normally used to store SIMILAR data types

let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
let details = ["John", "Smith", 32, true]
console.log(details);

// Objects are another special kind of data type that's used to mimic real world objects/items
		// They're used to create complex data that contains pieces of information that are relevant to each other
		// Every individual piece of information is called a property of the object

let person = {
    fullName: "Bilbo Baggins",
    age: 90,
    isMarried: false,
    contact: ["0912 3456 789", "1234 5678"],
    address: {
        houseNumber: '123',
        city: "Shire"
    }
}

console.log(person);

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6,
}

console.log(myGrades);

console.log(typeof myGrades);
// Not: Array is a special type of object with methods and functions to manipulate it.
console.log(typeof grades);

const anime = ["Madoka Magica", "Parayste", "One Piece"]
anime[0] = "meteor garden"
console.log(anime);

// NULL
// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
let spouse = null, myNumber = 0, myString = ''

console.log(spouse);
console.log(myNumber);
console.log(myString);

// UNDEFINED
// Represents the state of a variable that has been declared but without an assigned value
console.log("Feelings is " + feelings);

// Undefined vs Null
		// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
		// null means that a variable was created and was assigned a value that does not hold any value/amount
		// Certain processes in programming would often return a "null" value when certain tasks results to nothing