// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: [
        "Pikachu",
        "Charizard",
        "Squirtle",
        "Bulbasaur",
    ],
    friends: {
        hoenn: [
            "May",
            "Max"
        ],
        kanto: [
            "Brock",
            "Misty"
        ]
    },
    talk: () => {
        return "Pikachu! I choose you!"
    }
};

let fainted;
function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    
    this.tackle = function(pokemonEnemy){
        let tackled = this.name + " tackled " + pokemonEnemy.name + "\n";
        let remainHealth =  pokemonEnemy.name + "'s health is now reduced to " + (pokemonEnemy.health - this.attack);
        
        pokemonEnemy.health = pokemonEnemy.health - this.attack;
        
        if(pokemonEnemy.health <= 0){
            this.faint(pokemonEnemy)
        }
        return pokemonEnemy.health <= 0 ? tackled + remainHealth + "\n" + fainted : tackled + remainHealth
    }
    
    this.faint = function(pokemonEnemy){
        fainted = pokemonEnemy.name + " fainted";
    }
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
console.log(trainer.talk());

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100)

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

console.log(geodude.tackle(pikachu));

console.log(pikachu);

console.log(mewtwo.tackle(geodude));

console.log(geodude);






//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}