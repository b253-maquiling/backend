const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/details", (req, res) => {
  /*  userController
    .getProfile(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err)); */

  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.post("/enroll", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let data = {
    userId: req.body.userId,
    courseId: req.body.courseId,
  };

  userController
    .enroll(data, userData)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      console.error(err);
      res.status(500).send({ error: "Internal server error" });
    });
});

module.exports = router;
