const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  courseController
    .addCourse(req.body, userData.isAdmin)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/all", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .getAllCourses()
        .then((resultFromController) => res.send(resultFromController));
    } else {
      res.send(false);
    }
  } catch (error) {
    console.log(error);
  }
});

router.get("/", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/:id", (req, res) => {
  courseController
    .getCourse(req.params.id)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.put("/:id", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .updateCourse(req.body, req.params.id)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  } catch (error) {
    console.log(error);
  }
});

router.patch("/:id/archive", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .archieveCourse(req.body, req.params.id)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
