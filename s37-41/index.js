const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

mongoose.connect(
  `mongodb+srv://admin:admin123@batch-253-maquiling.vubwuh4.mongodb.net/course-booking?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

db.once("open", () => console.log("Now Connected to MongoDB atlas"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/courses", courseRoute);

if (require.main === module) {
  app.listen(port, () => {
    console.log(`API is now online on port ${port}`);
  });
}

module.exports = app;
