const bcrypt = require("bcrypt");

const auth = require("../auth");
const User = require("../models/User");
const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email })
    .then((result) => {
      if (result.length > 0) {
        return true;
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser
    .save()
    .then((user) => (user ? true : false))
    .catch((err) => err);
};

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((result) => {
      if (result === null) {
        return false;
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );

        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return false;
        }
      }
    })
    .catch((err) => err);
};

module.exports.getProfile = (reqBody) => {
  return User.findById(reqBody.userId)
    .then((result) => {
      result.password = "";
      return result;
    })
    .catch((err) => err);
};

module.exports.enroll = async (data, userData) => {
  let userId = userData.id;
  if (!userData.isAdmin) {
    let isUserUpdated = await User.findById(userId).then((user) => {
      user.enrollments.push({ courseId: data.courseId });
      return user
        .save()
        .then((user) => true)
        .catch((err) => false);
    });

    let isCourseUpdated = await Course.findById(data.courseId).then(
      (course) => {
        course.enrollees.push({ userId: userId });
        return course
          .save()
          .then((course) => true)
          .catch((err) => false);
      }
    );

    if (isUserUpdated && isCourseUpdated) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
};
