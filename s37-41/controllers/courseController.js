const Course = require("../models/Course");

module.exports.addCourse = (reqBody, isAdmin) => {
  if (isAdmin) {
    let newCourse = new Course({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    });

    return newCourse
      .save()
      .then((course) => true)
      .catch((err) => false);
  } else {
    return Promise.reject(false);
  }
};

module.exports.getAllCourses = () => {
  return Course.find({})
    .then((result) => result)
    .catch((err) => err);
};

module.exports.getAllActive = () => {
  return Course.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

module.exports.getCourse = (courseId) => {
  return Course.findById(courseId)
    .then((result) => result)
    .catch((err) => err);
};

module.exports.updateCourse = (reqBody, courseId) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };
  return Course.findByIdAndUpdate(courseId, updatedCourse)
    .then((result) => true)
    .catch((err) => err);
};

module.exports.archieveCourse = (reqBody, courseId) => {
  let statusCourse = {
    isActive: reqBody.isActive
  }
  return Course.findByIdAndUpdate(courseId, statusCourse)
    .then((result) => true)
    .catch((err) => err);
};
