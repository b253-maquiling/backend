// CRUD Operations
/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
		*/

// INSERT

/* 

Insert One Document
db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});
            
            Insert Many Documents
			db.collectionName.insertMany([
                {
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
                    "fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);
            
            */
db.users.insertOne({
  firstName: "Damiel",
  lastName: "Pogi",
  mobileNumber: "0987654321",
  email: "d@gmail.com",
  company: "Zuitt",
});

db.users.insertMany([
  {
    firstName: "Aoyama",
    lastName: "Nagisa",
    age: 21,
    contact: {
      phone: "12345678",
      email: "anagisa@gmail.com",
    },
    courses: ["Python", "PHP", "CSS"],
    department: "none",
  },
  {
    firstName: "Sayuri",
    lastName: "Date",
    age: 22,
    contact: {
      phone: "345567689",
      email: "sdati@gmail.com",
    },
    courses: ["React", "PHP", "SASS"],
    department: "none",
  },
]);

db.courses.insertMany([
  {
    name: "Javascript 101",
    price: 5000,
    description: "Introduction to Javascript",
    isActive: true,
  },
  {
    name: "HTML 101",
    price: 2000,
    description: "Introduction to HTML",
    isActive: true,
  },
]);

// Finding Documents (Read/Retrieve)
/* 
    Syntax:
    - db.collectionName.find(); -find all
    - db.collectionName.find({field: value}) - all document that will match the criteria
    - db.collectionName.findOne({field: value}) - first document that will match the criteria
    - db.collectionName.findOne({}) - find first document
*/

db.users.find();
db.users.find({ firstName: "Aoyama" });

// Updating/Replacing/Modifying documents (Update)
/* 
    Syntax:
    Updating One Document
        db.collectionName.updateOne(
            {
                field: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );
         - update the first matching document in our collection

    Multiple/Many Documents
    db.collectionName.updateMany(
        {},
        {
            $set: {
                fieldToBeUpdated: value
            }
        }
    );

     -update multiple documents that matched the criteria
*/

db.users.insertOne({
  firstName: "Test",
  lastName: "Test",
  mobileNumber: "0987654321",
  email: "t@gmail.com",
  company: "Zuitt",
});

db.courses.updateOne(
  {
    name: "HTML 101",
  },
  {
    $set: {
      //$rename $unset
      isActive: false,
    },
  }
);

db.users.updateMany(
  {},
  {
    $rename: {
      //$set $unset
      department: "dept",
    },
  }
);

// Remove Field
db.users.updateOne(
  {
    firstName: "Aoyama",
  },
  {
    $unset: {
      status: "active",
    },
  }
);

// Deleting Documents
/* 
  Syntax:
    Deleting One Document:
        db.collectionName.deleteOne({"critera": "value"})

    Deleting Multiple Documents:
        db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteOne({
  lastName: "Nagisa",
});

db.users.deleteMany({
  department: "none",
});

db.users.deleteMany({});

// TEST

db.testing.insertMany([
  {
    firstName: "Aoyama",
    lastName: "Nagisa",
    age: 21,
    contact: {
      phone: "12345678",
      email: "anagisa@gmail.com",
    },
    courses: ["Python", "PHP", "CSS"],
    department: "none",
  },
  {
    firstName: "Sayuri",
    lastName: "Date",
    age: 22,
    contact: {
      phone: "345567689",
      email: "sdati@gmail.com",
    },
    courses: ["React", "PHP", "SASS"],
    department: "none",
  },
]);

db.testing.updateOne(
  {
    firstName: "Sayuri",
  },
  { $set: {
    lastName: "Yabushima",
    age: 25,
    courses: ["C#, C++, Unity"]
  } }
);

db.testing.updateOne(
    {
        firstName: "Aoyama",
    },
    {
        $rename: {
            age: "edad"
        }
    }
)
