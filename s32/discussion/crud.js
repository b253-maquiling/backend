let http = require("http");

// Mock database
let directory = [
  {
    name: "Brandon",
    email: "brandon@mail.com",
  },
  {
    name: "Jobert",
    email: "jobert@mail.com",
  },
];
http
  .createServer((req, res) => {
    if (req.url === "/users" && req.method === "GET") {
      // Requests the "/users" path and "GETS" information
      // Sets the status code to 200, denoting OK
      // Sets response output to JSON data type
      res.writeHead(200, { "Content-TypeL": "application/json" });
      // Input HAS to be data type STRING hence the JSON.stringify() method
      // This string input will be converted to desired output data type which has been set to JSON
      // This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
      res.write(JSON.stringify(directory));
      res.end();
    } else if (req.url == "/users" && req.method == "POST") {
      let requestBody = "";
      // A stream is a sequence of data
      // Data is received from the client and is processed in the "data" stream
      // The information provided from the request object enters a sequence called "data" the code below will be triggered
      // data step - this reads the "data" stream and processes it as the request body
      req.on("data", (data) => {
        // Assigns the data retrieved from the data stream to requestBody
        requestBody += data;
      });
      // response end step - only runs after the request has completely been sent
      req.on("end", () => {
        // Check if at this point the requestBody is of data type STRING
        // We need this to be of data type JSON to access its properties
        console.log(typeof requestBody);
        // Converts the JSON string requestBody to JS Object
        requestBody = JSON.parse(requestBody);

        let newUser = {
          name: requestBody.name,
          email: requestBody.email,
        };
        directory.push(newUser);
        console.log(directory);

        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify(newUser));
        res.end();
      });
    }
  })
  .listen(4000);

console.log("Server running at localhost:4000");
