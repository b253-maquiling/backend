//ARRAY
// Array in programming is simply a list of data

let studentNumberA = "2023-1921"
let studentNumberB = "2023-1922"
let studentNumberC = "2023-1923"
let studentNumberD = "2023-1924"

let studentNumbers = [ "2023-1921", "2023-1922", "2023-1923", "2023-1924" ]

/*
	- Arrays are used to store multiple related values in a single variable
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
	- Arrays also provide access to a number of functions/methods that help in achieving this
	- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
	- Arrays are also objects which is another data type
	- The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
*/


let grades = [98.5, 94.3, 89.2, 90.1] //number array
let computerBrands = ["Acer", "Asus", "Lenovo", "MSI"]//string array


//Alternative way to write arrays
let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
]

let city1 = "Tokyo", city2 = "Manila", city3 = "Berlin"

let cities = [city1,city2,city3];
console.log(myTasks);
console.log(cities);

console.log(myTasks.length);
console.log(cities.length);

let fullname = "Aoyama Nagisa"
console.log(fullname.length);

let blankArray =[]
console.log(blankArray.length);

// deleting last item of an array
myTasks.length = myTasks.length-1
console.log(myTasks.length);
console.log(myTasks);

//using decrementation
cities.length--
console.log(cities);


// we cannot delete using length propert in strings
fullname.length = fullname.length-1
console.log(fullname.length);

fullname.length--
console.log(fullname);

// Lengthen Array
let theBeatles = ["John", "Paul", "Ringo", "George"]
console.log(theBeatles.length);
theBeatles.length++
console.log(theBeatles);
console.log(theBeatles.length-1);

theBeatles[4]= "Yaboshima Mie"
console.log(theBeatles);

console.log(grades[20]);
grades[20] = 90.5;
console.log(grades[20]);
console.log(grades);


let kaponanNiEugene = ["Eugene", "Vincent", "Alfred", "Dennis"]
console.log(kaponanNiEugene[3]);
console.log(kaponanNiEugene[1]);

let member = kaponanNiEugene[2];
console.log(member);

console.log("Array befoe reassignment:");
console.log(kaponanNiEugene);
kaponanNiEugene[2] = "Jeremiah"
console.log("Array after reassignment");
console.log(kaponanNiEugene);

let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]
let lastElementIndex = bullsLegend.length - 1;
console.log(bullsLegend[lastElementIndex]);
console.log(bullsLegend[bullsLegend.length - 1]);
console.log(bullsLegend[bullsLegend.length - 2]);


let newArr = []
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Sayuri Date"
console.log(newArr);
newArr[1] = "Liyuu"
console.log(newArr);

console.log(newArr.length);
newArr[newArr.length] = "Amamiya Sora"
console.log(newArr);
console.log(newArr.length);
newArr[newArr.length] = "Payton Naomi"

let superHeroes = ["Iron-man", "Speder-Man", "Captain America"]

function marvelHeroes(heroName) {
    superHeroes[superHeroes.length] = heroName
}

marvelHeroes("Ant-Man")
console.log(superHeroes);
marvelHeroes("Scarlet Witch")
console.log(superHeroes);

function getHeroByIndex(index) {
    return superHeroes[index]
}

let heroFound = getHeroByIndex(2)
console.log(heroFound);

//Loops over an Array
for(let index = 0; index < newArr.length; index++){
    console.log(newArr[index]);
}

let numArr = [5,12,30,46,50,98]
for(let index = 0; index < numArr.length; index++){
    if (numArr[index] % 5 === 0) {
        console.log(numArr[index] + " is divisible by 5.");
    } else {
        console.log(numArr[index] + " is not divisible by 5.");
    }
}

// Multidimensional Array

let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],

]
console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[7][4]);