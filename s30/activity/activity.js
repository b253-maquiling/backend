db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $count: "fruitsOnSale",
  },
]);

db.fruits.aggregate([
  {
    $match: {
      stock: { $gte: 20 },
    },
  },
  {
    $count: "enoughStock",
  },
]);

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $avg: "$price" },
    },
  },
  {
    $sort: {
      total: -1,
    },
  },
]);

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $max: "$price" },
    },
  },
  {
    $sort: {
      total: 1,
    },
  },
]);

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $min: "$price" },
    },
  },
  {
    $sort: {
      total: 1,
    },
  },
]);
